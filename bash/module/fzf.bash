WRK_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# fzf configuration
[ ! -f ~/.fzf.bash ] && bash ${WRK_DIR}/externals/fzf/install
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

if [[ -f ~/.fzf.bash ]]
then
    export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
    export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
fi

