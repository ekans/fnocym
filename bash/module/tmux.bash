if ! hash tmux 2>/dev/null
then
    echo "Tmux module not loaded"
    return
fi

send_bottom() {
    cmd=$@
    tmux send-keys -t bottom "$cmd" C-m
}
send_right() {
    cmd=$@
    tmux send-keys -t right "$cmd" C-m
}
send_pwd() {
    pwd=$(pwd)
    tmux send-keys -t bottom "cd $pwd" C-m C-l
}
send_clear() {
    pwd=$(pwd)
    tmux send-keys -t bottom C-l
}

echo "tmux.bash loaded"
